<?php

/**
 * This file is part of the Stream\Filesystem Package
 *
 * (c) Thomas Appel <mail@thomas-appel.com
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Filesystem;

use Stream\Common\Interfaces\InterfaceToArray;
use Stream\Filesystem\Exception\FSIOException;
use Stream\Filesystem\Exception\FSObjectInitialisationException;
use Stream\Filesystem\MIME\InterfaceMimeSniffer;

/**
 * FSFile
 *
 * @uses FSObject
 * @package Stream\Filesystem
 * @version 1.0
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 *
 * @property-read string   $name       File name
 * @property-read string   $path       File path
 * @property-read string   $extension  Fileextension
 * @property-read string   $mimetype   Mimetype
 * @property-read string   $uid        Userid
 * @property-read string   $gid        Groupid
 * @property-read string   $owner      Owner
 * @property-read string   $group      Group
 * @property-read string   $perm       Permission level
 */
class FSFile extends AbstractFSObject implements InterfaceToArray
{

    /**
     * Instance of an MimeSniffer implementation
     *
     * @var Stream\Filesystem\Mime\Interfaces\MimeSniffer
     * @STREAM\require()
     * @access protected
     */
    protected static $mimesniffer;

    /**
     * __construct
     *
     * @param string  $path  Path of this file
     * @access public
     *
     * @throws FSObjectInitialisationException
     */
    public function __construct($path)
    {
        if (!isset(static::$mimesniffer) && !static::setMimeSniffer()) {
            throw new FSObjectInitialisationException("Cannot initialize item, mimesniffer is not declared");
        }
        parent::__construct($path);
    }

    /**
     * set the mime type sniffer object
     *
     * @param InterfaceMimeSniffer $mimesniffer instance
     * of Stream\Filesystem\Mime\Interfaces\MimeSniffer implementation
     * @static
     * @access public
     * @return void
     */
    public static function setMimeSniffer(InterfaceMimeSniffer $mimesniffer = null)
    {
        if (!is_null($mimesniffer)) {
            static::$mimesniffer = $mimesniffer;
            return true;
        }

        $sniffer = static::detectMimeSniffer();
        static::$mimesniffer = new $sniffer;
        return true;
    }

    /**
     * detetct which mimesniffer implementaion should be used
     *
     * @static
     * @access protected
     * @return void
     */
    protected static function detectMimeSniffer()
    {
        if (function_exists('finfo_open') && defined('FILEINFO_MIME_TYPE')) {
            return 'Stream\Filesystem\MIME\MIMEFinfo';
        }
        if (function_exists('mime_content_type')) {
            return 'Stream\Filesystem\MIME\MIMEContentType';
        }
        return 'Stream\Filesystem\MIME\MIMEGeneric';
    }

    /**
     * returns an array containing all file attributes
     *
     * @access public
     * @return array
     */
    public function toArray()
    {
        return $this->attributes;
    }

    /**
     * Move a file to a differet location
     *
     * @param  string  $location   path of the new file location
     * @param  boolean $enum       determine if the file should be enumerated
     *  using the `$enumPrefix` parameter if it already exists at the new
     *  location
     * @param  int     $enumBase   enumeration start value
     * @param  string  $enumPrefix enumeration prefix
     * @access public
     * @return string|boolean
     */
    public function move($location, $enum = true, $enumBase = 1, $enumPrefix = 'copy')
    {
        return $this->moveFile(static::normalizePath($location), $enum, $enumBase, $enumPrefix, false);
    }

    /**
     * copy
     *
     * @param mixed $newname
     * @access public
     * @return string|boolean
     */
    public function copy($newname = null)
    {
        return $this->moveFile(is_null($newname) ? $this->path : static::normalizePath($newname), true, 1, 'copy', true);
    }

    /**
     * moveFile
     *
     * @param mixed $location
     * @param mixed $enum
     * @param mixed $enumBase
     * @param mixed $enumPrefix
     * @param mixed $copy
     * @access private
     * @return string|boolean
     */
    private function moveFile($location, $enum, $enumBase, $enumPrefix, $copy)
    {
        $destination = dirname($location);
        $wasNameReplaced = false;

        if (!is_dir($destination)) {
            return false;
        }

        if (is_dir($location)) {
            $nfile = $location . static::DIRECTORY_SEPARATOR . $this->name;
            $path = $enum ? $this->enum($nfile, $enumBase, $enumPrefix) : $nfile;

            if (!file_exists($path) && rename($this->path, $path)) {
                $this->setAttributes($path);
                return true;
            }
        } else {
            $path = $enum ? $this->enum($location, $enumBase, $enumPrefix) : $location;
            $wasNameReplaced = $enum !== $location;
            if (false === $copy && !file_exists($path) && ($this->isUploadedFile() ?
                move_uploaded_file($this->path, $path) :
                rename($this->path, $path))) {
                $this->setAttributes($path);
                return $wasNameReplaced ? $path : true;
            } elseif ($copy && copy($this->path, $path)) {
                return $wasNameReplaced ? $path : true;
            }
        }
        return false;
    }

    /**
     * Rename a the file.
     *
     * Will return true if file already exists, otherwhise
     * true. If you want to move the file to a different location
     * see `FSFile::move()`
     *
     * @param string $name The new file name
     * @access public
     * @return Boolean
     */
    public function rename($name)
    {
        $name = basename($name);
        $path = sprintf("%s%s%s", dirname($this->path), static::DIRECTORY_SEPARATOR, $name);

        if (file_exists($path)) {
            return false;
        }

        if (rename($this->path, $path)) {

            $this->setAttributes($path);

            return true;
        }
        return false;
    }

    public function lastModified()
    {
        return filemtime((string)(this));
    }

    /**
     * Determine if this file exists
     *
     * @access public
     * @return void
     */
    public function exists()
    {
        return file_exists($this);
    }

    /**
     * Implementation of FSObject::isFile()
     *
     * @access public
     * @return Boolean
     */
    public function isFile()
    {
        return $this->exists();
    }

    /**
     * Implementation of FSObject::isDir()
     *
     * @access public
     * @return Boolean false
     */
    public function isDir()
    {
        return false;
    }

    /**
     * check if the file was uploaded via http POST
     *
     * @access public
     * @return boolean
     */
    public function isUploadedFile()
    {
        return is_uploaded_file((string)$this);
    }

    /**
     * remove this file
     *
     * @access public
     * @return boolean
     */
    public function remove()
    {
        if (unlink((string)$this)) {
            unset($this);
            return true;
        }

        return false;
    }

    /**
     * Write to file.
     * See `file_put_contents` for possible operation flags.
     *
     * @see <http://www.php.net/manual/en/function.file-put-contents.php>
     *
     * @param String   $content  Filecontent as String
     * @param Constant $flags    Write Flags
     * @access public
     * @return Mixed returns the number of bytes that were written to the file, or false on failure.
     */
    public function put($content, $flags = LOCK_EX)
    {
        return file_put_contents($this->path, $content, $flags);
    }

    /**
     * Get file contents.
     *
     * @see <http://www.php.net/manual/en/function.file-get-contents.php>
     *
     * @param int   $offset  file read start possition offset
     * @param Mixed $maxlen  maximum length to be read
     * @access public
     * @return Mixed returns the read file of false on failure
     */
    public function contents($offset = 0, $maxlen = null)
    {
        $args = [(string)$this, $offset, null, null];

        if (!is_null($maxlen)) {
            $args[] = $maxlen;
        }

        return call_user_func_array('file_get_contents', $args);
    }

    /**
     * get files attributes of a file
     *
     * @param  string $path filepath
     * @access protected
     * @return void
     */
    protected function getAttributes($file)
    {
        $this->path = $file;
        $uid = fileowner($file);
        $gid = filegroup($file);

        return [
            'path'      => $file,
            'name'      => basename($file),
            'uid'       => $uid,
            'gid'       => $gid,
            'owner'     => $this->getUserName($uid),
            'group'     => $this->getGroupName($gid),
            'perm'      => substr(sprintf('%o', fileperms($file)), -4),
            'extension' => strtolower(pathinfo($file, PATHINFO_EXTENSION)),
            'mimetype'  => static::$mimesniffer->getMime($file),
        ];
    }

    /**
     * enumerates a filename that already exists.
     *
     * @see Stream\Filesystem\FSObject::enum()
     */
    protected function enum($file, $start, $prefix = null, $pad = true)
    {
        if (!file_exists($file)) {
            return $file;
        }

        $prefix = is_null($prefix) ?  $prefix : ($pad ? str_pad($prefix, strlen($prefix) + 2, ' ', STR_PAD_BOTH) : $prefix);

        $name      = pathinfo($file, PATHINFO_FILENAME);
        $path      = dirname($file) . static::DIRECTORY_SEPARATOR;
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        $extension = strlen($extension) ? ".$extension" : '';

        $i = $start;

        while (file_exists(sprintf("%s%s%s%d%s", $path, $name, $prefix, $i, $extension))) {
            $i++;
        }

        return sprintf("%s%s%s%d%s", $path, $name, $prefix, $i, $extension);
    }

    /**
     * create new file if it doesn't exist.
     *
     * @param string $path file path
     * @param number $perm file permission
     * @access protected
     * @return boolean|void
     *
     * @throws IOException
     */
    protected function create($path)
    {
        if (file_exists($path)) {
            return;
        }
        throw new FSIOException($path . ': No such file');
    }
}
