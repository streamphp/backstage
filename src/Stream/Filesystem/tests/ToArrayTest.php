<?php

/**
 * This File is part of the Stream\Filesystem package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Filesystem;

use Stream\Filesystem\AbstractFSObject;
use Stream\Filesystem\FSDirectory;
use Stream\Filesystem\FSFile;
use org\bovigo\vfs\vfsStream;

/**
 * Class: ToArrayTest
 *
 * @see PHPUnit_Framework_TestCase
 */
class ToArrayTest extends DirectoryProvider
{
    protected $root;
    protected $path;
    protected $object;

    protected function setUp()
    {
        $struct = current(current($this->directoryProvider()));
        $keys = array_keys($struct);

        $this->root = vfsStream::setup('testpath', 777);
        vfsStream::create($struct, $this->root);
        $this->path = vfsStream::url('testpath/' . $keys[0]);
    }

    /**
     * @test
     * @covers FSDirectory::__construct
     */
    public function testIterate()
    {
        $dir = new FSDirectory($this->path);
        $i = 1000;

        while ($i > 0) {
            $dir->toArray();
            $i--;
        }
    }
}
