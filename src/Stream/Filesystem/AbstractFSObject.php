<?php

/**
 * This file is part of the Stream\Filesystem Package
 *
 * (c) Thomas Appel <mail@thomas-appel.com
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Filesystem;

use Stream\Filesystem\Exception\FSObjectInitialisationException;
use Stream\Filesystem\Exception\FSIOException;
use Stream\Filesystem\MIME\InterfaceMimeSniffer;

/**
 * FSObject
 *
 * @implements InterfaceFSObject
 * @abstract
 *
 * @package Stream\Filesystem
 * @version 1.0
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
abstract class AbstractFSObject implements InterfaceFSObject
{
    /**
     * @var string
     * @access protected
     */
    const DIRECTORY_SEPARATOR = '/';

    /**
     * The File’s attributes
     *
     * @var Array
     * @access protected
     */
    protected $attributes;

    /**
     * Determines if the file has changes attributes
     *
     * @var Boolean
     * @access protected
     */
    protected $changed;

    /**
     * Copy of the original Attributes
     *
     * @var Array
     * @access protected
     */
    protected $originalAttributes;

    /**
     * Array keys of changed attributes
     *
     * @var Array
     * @access protected
     */
    protected $changedAttributes;

    /**
     * path
     *
     * @var mixed
     */
    protected $path;

    /**
     * __construct
     *
     * @param string  $path String representation of the file's path
     * @param number  $perm  Default permission.
     * @access public
     *
     * @throws FSObjectInitialisationException
     */
    public function __construct($path)
    {
        $this->path = static::normalizePath($path, false);
        $this->create($this->path);
        $this->changed = false;
    }

    /**
     * return the file's path
     *
     * @access public
     * @return String absolute path of this object
     */
    public function __toString()
    {
        return $this->path;
    }

    /**
     * Getter for the attributes array
     *
     * @param Mixed $prop
     * @access public
     * @return Mixed null or array
     */
    public function __get($prop)
    {
        $this->ensureAttributes();

        if (isset($this->attributes[$prop])) {
            return $this->attributes[$prop];
        }
    }

    /**
     * Setter for the attributes array
     *
     * Triggers attribute changes
     *
     * @param string $attribute attribute name
     * @param string $value     attribute value
     * @access protected
     * @return void
     */
    protected function set($attribute, $value)
    {
        if (!$this->changed) {
            $this->ensureAttributes();
            $this->originalAttributes = $this->attributes;
            $this->changedAttributes = [];
            $this->changed = true;
        }

        !in_array($attribute, $this->changedAttributes)
            && array_push($this->changedAttributes, $attribute);

        $this->attributes[$attribute] = $value;
    }

    /**
     * Move a file to a differet location
     *
     * @param string $location    filepath to move file to
     * @param bool   $enum        enumerate file in case it alread exists
     * @param int    $enumBase    enumeration start value, default `1`
     * @param bool   $enumPrefix  enumeration prefix, default `' copy '`
     * @abstract
     * @access public
     * @return string the new file path
     */
    abstract public function move($location, $enum = true, $enumBase = 1, $enumPrefix = 'copy');

    /**
     * rename a file
     *
     * @param string $name The new file name
     * @abstract
     * @access public
     * @return void
     */
    abstract public function rename($name);


    /**
     * Check if is file
     *
     * @abstract
     * @access public
     * @return Boolean
     */
    abstract public function isFile();

    /**
     * isDir
     *
     * @abstract
     * @access public
     * @return Boolean
     */
    abstract public function isDir();

    /**
     * Get the original attributes of the file or directory
     *
     * @param  string|null $attribute File attribute as described in `FSObject::$attributes`
     * @access public
     * @return array|string the changed attribute or all original attributes as array
     */
    public function getOriginalAttributes($attribute = null)
    {
        $this->ensureAttributes();

        if (is_null($attribute)) {
            return $this->changed ? $this->originalAttributes : $this->attributes;
        }
        if ($this->changed && $this->{$attribute}) {
            return $this->originalAttributes[$attribute];
        }
        return $this->{$attribute};
    }

    /**
     * ensureAttributes
     *
     * @access protected
     * @return void
     */
    protected function ensureAttributes()
    {
        if (is_null($this->attributes)) {
            $this->setAttributes();
        }
    }

    /**
     * Change or retreive file permsission
     *
     * Returns current permission setting if $mode is omitted.
     *
     * @param  int  $mode  Octal access representation
     * @access public
     * @return boolean|integer true if permission was altererd or current
     *  permission ($this->perm).
     *
     *  @throws FSIOException
     */
    public function permission($mode = null, $clearstat = true)
    {
        if (is_null($mode)) {
            return $this->perm;
        }
        $this->convertPermission($mode);

        if (!@chmod((string)$this, $mode)) {
            throw new FSIOException(sprintf("cannot change permission on %s to %04o", $this->path, $mode));
        }

        if ($clearstat) {
            clearstatcache(true, $this->path);
        }
        $this->set('perm', null);
        $this->setAttributes($this->path);
        return true;
    }

    /**
     * lastModified
     *
     * @access public
     * @return mixed
     */
    public function lastModified()
    {
        return filemtime($this->path);
    }

    /**
     * Change permission.
     *
     * @param Mixed $mode
     * @param Mixed $clearstat
     * @access public
     * @return Boolean
     */
    public function chmod($mode, $clearstat = true)
    {
        return $this->permission($mode, $clearstat);
    }

    /**
     * Change owner.
     *
     * @param Mixed   $ownerID   set owner id
     * @param Mixed $clearstat
     * @access public
     * @return void
     */
    public function chown($ownerID, $clearstat = true)
    {
        if (!$this->uidExists($ownerID)) {
            return false;
        }

        return $this->doChown($ownerID, $clearstat);

    }

    /**
     * Change group.
     *
     * @param Mixed $groupID
     * @param Mixed $clearstat
     * @access public
     * @return booelan
     */
    public function chgrp($groupID, $clearstat = true)
    {
        if (!$this->gidExists($groupID)) {
            return false;
        }

        return $this->doChgrp($groupID, $clearstat);
    }

    /**
     * isRelativePath
     *
     * @param mixed $path
     * @access public
     * @return boolean
     */
    public static function isRelativePath($path)
    {
        return $path && !empty($path) && !(bool)preg_match('/^(\w+\:(\\\|\\/){1,3}|\\/)/im', $path);
    }

    /**
     * normalizePath
     *
     * @param mixed $path
     * @access public
     * @return mixed
     */
    public static function normalizePath($path, $abstract = true)
    {
        if (false === $abstract) {
            $path = static::getRealAbstractPath($path, true);
            $path = $path ? $path : current(func_get_args());
        }
        return str_replace('\\', static::DIRECTORY_SEPARATOR, $path);
    }

    /**
     * getRealAbstractPath
     *
     * @param mixed $path
     * @access public
     * @return string|null
     */
    public static function getRealAbstractPath($path, $forcerelative = false)
    {
        if (static::isRelativePath($path) && !$forcerelative) {
            $path = getcwd() . static::DIRECTORY_SEPARATOR . $path;
        }
        $path = static::normalizePath($path);

        if (realpath($path)) {
            return realpath($path);
        }

        preg_match('/^(\w+\:\\/{1,3}|\\/)/im', $path, $matches);

        $prefix = empty($matches) ? '' : $matches[0];
        $path = substr($path, strlen($prefix));
        $parts = explode(static::DIRECTORY_SEPARATOR, $path);
        $out = [];

        while (count($parts) > 0) {
            $part = array_shift($parts);
            switch (true) {
            case $part === '.' || (0 === strlen(trim($part)) && count($parts) > 0):
                continue;
            case $part === '..':
                array_pop($out);
                continue;
            default:
                $out[] = $part;
                break;
            }
        }
        return $prefix . preg_replace('/\/\.$/', '', implode(static::DIRECTORY_SEPARATOR, $out));
    }


    /**
     * Run the chown command called in $this->chown;
     *
     * @param Mixed $ownerID
     * @param Boolean $clearstat
     * @access protected
     * @return boolean
     *
     * @throws FSIOException
     */
    protected function doChown($ownerID, $clearstat)
    {
        if (!@chown((string)$this, $ownerID)) {
            throw new FSIOException(sprintf("cannot change ownership on %s to %s", $this->path, (string)$ownerID));
        }

        if ($clearstat) {
            clearstatcache(true, $this->path);
        }

        $this->setAttributes($this->path);

        return true;
    }

    /**
     * Run the chgrp command called in $this->chgrp;
     *
     * @param Mixed $groupID
     * @param Boolean $clearstat
     * @access protected
     * @return Boolean
     *
     * @throw FSIOException
     */
    protected function doChgrp($groupID, $clearstat)
    {
        if (!@chgrp((string)$this, $groupID)) {
            throw new FSIOException(sprintf("cannot change group on %s to %s", $this->path, (string)$groupID));
        }

        if ($clearstat) {
            clearstatcache(true, $this->path);
        }

        $this->setAttributes($this->path);
        return true;
    }

    /**
     * Populate or update $this->attributes
     *
     * @param Mixed $path
     * @access protected
     * @return void
     */
    protected function setAttributes($path = null)
    {
        $file = is_null($path) ? $this->path : $path;
        $this->path = $file;
        $attributes = $this->getAttributes($file);

        if (is_null($this->attributes)) {
            $this->attributes = $attributes;
            return;
        }

        foreach ($attributes as $attr => $value) {
            if ($this->attributes[$attr] !== $value) {
                $this->set($attr, $value);
            }
        }
    }

    /**
     * Get the groupname by posix GID
     *
     * @param Integer $gid  the groupid
     * @access protected
     * @return string the groupname
     */
    protected function getGroupName($gid)
    {
        if ($guid = @posix_getgrgid($gid)) {
            return $guid['name'];
        }

        return 'unavailable';
    }

    /**
     * Get the username by posix UID
     *
     * @param integer $uid the userid
     * @access protected
     * @return string the username
     */
    protected function getUserName($uid)
    {
        if ($puid = @posix_getpwuid($uid)) {
            return $puid['name'];
        }

        return 'unavailable';
    }

    /**
     * Check if a posix GID exists
     *
     * @param Mixed $gid string or integer, weather the gid or groupname
     * @access protected
     * @return boolean
     */
    protected function gidExists($gid)
    {
        if (is_numeric($gid) && @posix_getgrgid((int)$gid) !== false) {
            return true;
        }

        return @posix_getgrnam($gid) !== false;
    }

    /**
     * Check if a posix UID exists
     *
     * @param string|integer $uid string or integer, weather the uid or username
     * @access protected
     * @return boolean
     */
    protected function uidExists($uid)
    {
        if (is_numeric($uid) && @posix_getpwuid((int)$uid) !== false) {
            return true;
        }

        return @posix_getpwnam($uid) !== false;
    }

    /**
     * Get file attributes
     *
     * @param string $file path to a file
     * @abstract
     * @access protected
     * @return array containing file attributes
     */
    abstract protected function getAttributes($file);

    /**
     * Get attributes that where cahnged since the Object initalization.
     *
     * @param string|null $attribute if null, returns all changed attributes
     * @access public
     * @return array|string|null the changed attribute or all changed attributes as array
     */
    /**
     * getChangedAttributes
     *
     * @param mixed $attribute
     * @access public
     * @return void
     *
     * @test     public function getChangedAttributes($attribute = null)
     */
    public function getChangedAttributes($attribute = null)
    {

        if (!$this->changed) {
            return [];
        }

        if (is_null($attribute)) {
            extract($this->attributes);
            return compact($this->changedAttributes);
        }

        return in_array($attribute, $this->changedAttributes) ? $this->{$attribute} : null;
    }

    /**
     * Converts octal value to an integer value
     *
     * @param integer|null $perm octal value
     * @access protected
     * @return integer
     */
    protected function convertPermission(&$perm = null)
    {
        $perm = is_string($perm) ? octdec($perm) : $perm;
    }
}
