<?php

/**
 * This file is part of the Stream\Filesystem Package
 *
 * (c) Thomas Appel <mail@thomas-appel.com
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Filesystem\MIME;

/**
 * GenericMimeSniffer
 *
 * @uses InterfaceMimeSniffer
 * @package Stream\Filesystem\Mime
 * @version 1.0
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class MIMEGeneric implements InterfaceMimeSniffer
{
    /**
     * @var array predefiened mimetypes;
     */
    protected static $mimetypes = array(

        'txt'  => 'text/plain',
        'htm'  => 'text/html',
        'html' => 'text/html',
        'php'  => 'text/html',
        'css'  => 'text/css',
        'js'   => 'application/javascript',
        'json' => 'application/json',
        'xml'  => 'application/xml',
        'swf'  => 'application/x-shockwave-flash',
        'flv'  => 'video/x-flv',

        // images
        'png'  => 'image/png',
        'jpe'  => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg'  => 'image/jpeg',
        'gif'  => 'image/gif',
        'bmp'  => 'image/bmp',
        'ico'  => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif'  => 'image/tiff',
        'svg'  => 'image/svg+xml',
        'svgz' => 'image/svg+xml',

        // archives
        'zip'  => 'application/zip',
        'rar'  => 'application/x-rar-compressed',
        'exe'  => 'application/x-msdownload',
        'msi'  => 'application/x-msdownload',
        'cab'  => 'application/vnd.ms-cab-compressed',

        'tar'  => 'application/x-tar',
        'gz'   => 'application/x-gzip',
        'bizp' => 'application/x-bzip',
        'bz2'  => 'application/x-bzip2',

        // audio/video
        'mp3'  => 'audio/mpeg',
        'qt'   => 'video/quicktime',
        'mov'  => 'video/quicktime',
        'mp4'  => 'video/mp4',
        'm4a'  => 'audio/mp4',
        'm4b'  => 'audio/mp4',
        'm4v'  => 'video/mp4',
        'ogv'  => 'video/ogg',
        'ogg'  => 'audio/ogg',
        'mkv'  => 'video/x-matroska',

        // adobe
        'pdf'  => 'application/pdf',
        'psd'  => 'image/vnd.adobe.photoshop',
        'ai'   => 'application/postscript',
        'eps'  => 'application/postscript',
        'ps'   => 'application/postscript',

        // ms office
        'doc'  => 'application/msword',
        'rtf'  => 'application/rtf',
        'xls'  => 'application/vnd.ms-excel',
        'ppt'  => 'application/vnd.ms-powerpoint',
        'docx' => 'application/msword',
        'xlsx' => 'application/vnd.ms-excel',
        'pptx' => 'application/vnd.ms-powerpoint',


        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
    );

    /**
     * {@inheritDoc}
     */
    public function getMime($path)
    {
        if ($mime = @`file --mime-type $path`) {
            return preg_replace("/^.*\:\s?/", "", $mime);
        }
        $extension = strtolower(pathinfo($path, PATHINFO_EXTENSION));

        if (isset(static::$mimetypes[$extension])) {
            return static::$mimetypes[$extension];
        }
        return 'application/octet-stream';
    }
}
