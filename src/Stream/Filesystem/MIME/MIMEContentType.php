<?php

/**
 * This file is part of the Stream\Filesystem Package
 *
 * (c) Thomas Appel <mail@thomas-appel.com
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Filesystem\MIME;

/**
 * ContentTypeMimeSniffer
 *
 * @uses InterfaceMimeSniffer
 * @package Stream\Filesystem\Mime
 * @version 1.0
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class MIMEContentType implements InterfaceMimeSniffer
{

    /**
     * {@inheritDoc}
     */
    public function getMime($path)
    {
        return mime_content_type($path);
    }
}
