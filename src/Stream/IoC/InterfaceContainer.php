<?php

/**
 * This File is part of the Stream\IoC package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\IoC;

use ArrayAccess;
use Closure;

/**
 * @interface InterfaceContainer
 */
interface InterfaceContainer extends ArrayAccess
{
    /**
     * Bind a class|object|callback to an identifier.
     *
     * Allows to bind different types of objects, e.g. bind an interface to
     * a specific implementation.
     *
     * @param string                     $identifier
     * @param null|closure|object|array  $implementation
     * @param boolean                    $shared
     * @throws ContainerBindException
     * @return void
     */
    public function bind($identifier, $implementation = null, $shared = false);

    /**
     * resolves an object
     *
     * If the the identifier wasn't previously bound and the identifier is an
     * existing class, `resolve` will interprete the identifier as class and try
     * to instanciate this class with all dependencies.
     *
     * @param string        $identifier  object identifier
     * @param mixed|array   $with        parameter to resolve with
     *
     * @throws ContainerResolutionException
     * @return mixed        typically a class instance
     */
    public function resolve($identifier, $with = []);

    /**
     * Bind an implementation as singleton
     *
     * @param string             $identifier
     * @param null|closure|class $implementation
     *
     * @return void
     */
    public function bindSingleton($identifier, $implementation = null);

    /**
     * Bind a shared object
     *
     * @param closure $callable
     * @access public
     * @return void
     */
    public function share(Closure $callable);

    /**
     * bind a closure as parameter instead of invoking it when it's resolved
     *
     * @param Closure $callable
     * @access public
     *
     * @return void
     */
    public function parameter(Closure $callable);

    /**
     * modify a bound object definition without actually calling it
     *
     * @access public
     * @return void
     */
    public function inject($identifier, Closure $callable);

    /**
     * addArgument
     *
     * @param mixed $argument
     * @access public
     * @return Container
     */
    public function addArgument($argument);

    /**
     * addSetter
     *
     * @param mixed $method
     * @param mixed $reference
     * @param mixed $binding
     * @access public
     * @return Container
     */
    public function addSetter($method, $reference, $binding = null);

    /**
     * hasSetters
     *
     * @param mixed $identifier
     * @access public
     * @return boolean
     */
    public function hasSetters($identifier);

    /**
     * hasArguments
     *
     * @param mixed $identifier
     * @access public
     * @return boolean
     */
    public function hasArguments($identifier);

    /**
     * return an array of identifiers that are bound to the container
     *
     * @access public
     * @return array
     */
    public function keys();
}
