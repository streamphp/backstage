# Stream IoC
## A Dependency injection Container
[![Build Status](https://api.travis-ci.org/streamphp/ioc.png?branch=master)](https://travis-ci.org/streamphp/ioc)

### Usage

##### Binding class factories (or whatever you want) 
```php
<?php

use Stream\IoC\Container;

$container = new Container();
$container->bind('resolvable', function () {
  $foo = new Foo();
  $class = new MyClassWithDependencies($foo);
  return $class;
});

$object = $container->resolve('resolvable');
$object instanceof MyClassWithDependencies; // => true   
```

##### Binding an interface to an Implementation

```php


$container->bind('MyInterface', function () {
  return new ClassImplementsMyInterface();
});

// or:

$container->bind('MyInterface', 'ClassImplementsMyInterface');
// or as singleton
$container->share('MyInterface', 'ClassImplementsMyInterface');

$object = $container->resolve('MyInterface');
$object instanceof MyInterface;                // => true
$object instanceof ClassImplementsMyInterface; // => true
```
##### Binding a class as singleton (or shared object)
```php

$container->bindSingleton('MyInterface', 'ClassImplementingMyInterface');

// or

$container['MyInterface'] = $container->share(function () {
	return new ClassImplementingMyInterface();
});

```

##### Resolving unbound classes

The IoC container will try to resolve a class with all its dependencies

```php


$object = $container->resolve('Myclass');
$object instanceof MyClass; // => true
```


