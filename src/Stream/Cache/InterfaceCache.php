<?php

/**
 * This File is part of the Stream\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Cache;

/**
 * Interface: InterfaceCache
 * @package Stream\Cache
 * @version 1.0
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
interface InterfaceCache
{
    /**
     * Retreive cached data by id
     *
     *
     * @param String $cacheid
     * @access public
     * @return Mixed
     * The cached data/object/array or null if no object was found
     */
    public function read($cacheid);

    /**
     * Write data to cache
     *
     * @param String $cacheid
     * @param Mixed  $data the data to be cached
     * @param string $expires a valid Date expression
     * @param mixed  $compressed
     * @access public
     * @return Boolean
     */
    public function write($cacheid, $data, $expires, $compressed = false);

    /**
     * Cache data forever
     *
     * @param mixed $cacheid
     * @param mixed $data
     * @param mixed $compressed
     */
    public function seal($cacheid, $data, $compressed = false);

    /**
     * Flush data from cache
     *
     * @param Mixed $cacheid
     * @access public
     * @return void
     */
    public function purge($cacheid = null);


    /**
     * Check if an item is already cached
     *
     * @param String $cacheid the cache item identifier
     * @access public
     * @return Boolean
     */
    public function has($cacheid);
}
