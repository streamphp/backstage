<?php
/**
 * This File is part of the Stream\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Cache;

use Stream\Cache\Storage;
use Stream\Cache\Driver\DriverRuntime;


/**
 * Class: StorageMemcachedTest
 *
 * @see \TestCases
 */
class StorageRuntimeTest extends StorageTestCases
{

    /**
     * setUp
     *
     * @access protected
     * @return void
     */
    protected function setUp()
    {
        $driver = new DriverRuntime();
        $this->cache = new Storage($driver, 'runtimecache');
    }

    /**
     * tearDown
     *
     * @access protected
     * @return void
     */
    protected function tearDown()
    {
        //$this->object->purge();
    }
}
