<?php

/**
 * This File is part of the Stream\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Cache;

use Stream\Tests\Cache\TestCases;

use Stream\Cache\Driver\DriverFilesystem;
use Stream\Cache\Storage;
use Stream\Filesystem\FSDirectory;
use Stream\Filesystem\FSFile;
use org\bovigo\vfs\vfsStream;

class StorageFileSystemTest extends StorageTestCases
{
    /**
     * @var ClassName
     */
    protected $cache;

    protected $path;

    protected $fs;

    protected function setUp()
    {
        $root = vfsStream::setup('testdir', 777);
        vfsStream::create([
                'tmp' => []
            ], $root);
        $tmp = vfsStream::url('testdir/tmp');
        $this->createTMPDIR($tmp);
        $driver = new DriverFilesystem($this->fs);
        $this->cache = new Storage($driver, 'fsys');
    }

    protected function createTMPDIR($dir)
    {
        $this->fs = new FSDirectory($dir);
    }

}
