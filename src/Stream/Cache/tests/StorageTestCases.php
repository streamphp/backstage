<?php

/**
 * This File is part of the Stream\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Cache;

use \StdClass;

/**
 * Class: TestCases
 *
 * @see \PHPUnit_Framework_TestCase
 * @abstract
 */
abstract class StorageTestCases extends \PHPUnit_Framework_TestCase
{

    /**
     * @test
     * @covers Storage#read()
     */
    public function testCacheReadShouldReturnNull()
    {
        $this->assertTrue(is_null($this->cache->read('cached_null')));
    }

    /**
     * @test
     * @covers Storage#write()
     * @covers Storage#read()
     */
    public function testCacheObjects()
    {
        $object = new StdClass();
        $object->name = 'testobj';

        $this->cache->write('cached_item', $object, 1000);
        $cachedObj = $this->cache->read('cached_item');
        $this->assertEquals($object, $cachedObj);

    }

    /**
     * @test
     * @covers Storage#write()
     * @covers Storage#read()
     */
    public function testCacheArrays()
    {
        $bar = 'bar';
        $array = array('foo' => &$bar);

        $this->cache->write('cached_array', $array, 1000);
        $cachedArray = $this->cache->read('cached_array');

        $this->assertSame($array, $cachedArray);
        $this->assertTrue($array === $cachedArray);
    }

    /**
     * @test
     * @covers Storage#write()
     * @covers Storage#read()
     */
    public function testCacheStrings()
    {
        $this->cache->write('cached_string', 'foobar', 1000);
        $this->assertSame('foobar', $this->cache->read('cached_string'));
    }

    /**
     * @test
     * @covers Storage#writeDefault()
     */
    public function testCacheWriteDefault()
    {
        $this->cache->writeDefault('cached_default', 1000, function () {
            return 'foobar';
        });

        $this->assertSame('foobar', $this->cache->read('cached_default'));
    }


    /**
     * @test
     * @covers Storage#has()
     */
    public function testHasCachedItem()
    {
        $this->cache->write('cached_string', 'foobar', 1000);
        $this->assertTrue($this->cache->has('cached_string'));
    }

    public function testCompressWriteToCache()
    {
        $this->cache->write('cached_string', 'foobar', 1000, true);
        $this->assertSame('foobar', $this->cache->read('cached_string'));
    }

    /**
     * @test
     * @skipTest
     * @covers Storage#write()
     * @covers Storage#read()
     */
    public function testPurgeCache()
    {
        $this->cache->write('cached_string_1', 'foobarbazo', 1000);
        $this->cache->write('cached_string_2', 'foobarboom', 1000);
        $this->cache->write('cached_string_3', 'foobarbara', 1000);

        $this->assertTrue($this->cache->has('cached_string_3'));

        $this->cache->purge('cached_string_3');

        $this->assertFalse($this->cache->has('cached_string_3'));
        $this->assertTrue($this->cache->has('cached_string_1'));
        $this->assertTrue($this->cache->has('cached_string_2'));

        $this->cache->purge();
        $this->assertFalse($this->cache->has('cached_string_1'));
        $this->assertFalse($this->cache->has('cached_string_2'));
    }
}
