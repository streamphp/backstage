<?php

/**
 * This File is part of the Stream\Logging package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Logging;

use Stream\Common\AbstractServiceDriver as ServiveDriver;

/**
 * @class LoggingServiceDriver
 */

class LoggingServiceDriver extends ServiveDriver
{
    /**
     * {@inheritDoc}
     */
    protected function registerService()
    {
        $app = $this->app;

        return [
            'log' => $this->app->share(
                function () use ($app) {
                    return new LogWriter(new \Monolog\Logger('log'), $app['filesystem'], 'application/logs');
                }
            )
        ];
    }
}
