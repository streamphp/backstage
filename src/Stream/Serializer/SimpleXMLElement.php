<?php

/**
 * This File is part of the ..\Serializer package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Serializer;

/**
 * Class: SimpleXMLElement
 *
 * @uses \SimpleXMLElement
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class SimpleXMLElement extends \SimpleXMLElement
{
    /**
     * boolish
     *
     * @var array
     */
    protected static $boolish = ['yes', 'no', 'true', 'false'];

    /**
     * argumentsAsArray
     *
     * @access public
     * @return array
     */
    public function attributesAsArray($namespace = null)
    {
        $attributes = [];
        $attr = $this->xpath('./@*');

        foreach ($attr as $key => $value) {
            $attributes[$key] = $this->getPhpValue((string)$value);
        }
        return $attributes;
    }

    /**
     * phpValue
     *
     * @access public
     * @return mixed
     */
    public function phpValue()
    {
        return $this->getPhpValue((string)$this);
    }

    /**
     * phpValue
     *
     * @param mixed $param
     * @access public
     * @return mixed
     */
    protected function getPhpValue($value)
    {
        switch (true) {
            case is_numeric($value):
                return ctype_digit($value) ? intval($value) : floatval($value);
            case in_array($value, static::$boolish):
                return ('false' === $value || 'no' === $value) ? false : true;
            default:
                return clear_value(trim($value));
        };

    }
}
