<?php

namespace Stream\Tests\Serializer;
use Stream\Serializer\Processors\XMLProcessor;
use DOMDocument;

class Test
{

    protected $data;
    protected $obj;

    /**
     * __construct
     *
     * @param array $data
     * @param \StdClass $obj
     */
    public function __construct(array $data = [], \StdClass $obj)
    {
        $this->data = $data;
        $this->obj  = $obj;
    }
}

class XMLProcessorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ClassName
     */
    protected $processor;

    protected function setUp()
    {
        $this->processor = new XMLProcessor();
        $this->processor->setPluralizer(function ($value) {
            if ($value == 'number') {
                return 'numbers';
            }
            if ($value == 'float') {
                return 'floats';
            }
            return $value;
        });
        $this->processor->setSingularizer(function ($value) {
            if ($value == 'numbers') {
                return 'number';
            }
            if ($value == 'floats') {
                return 'float';
            }
            return $value;
        });
    }

    /**
     * @test
     */
    public function encodeShouldReturnValidXMLFromFlatArray()
    {
        $data = ['foo' => 'bar'];
        $string = $this->processor->encode($data);
        $xml = simplexml_load_string($string);
        $childnode = $xml->children();
        $this->assertTrue($childnode->getName() === 'foo');
    }

    /**
     * @test
     * @markTestSkipped
     */
    public function encodeShouldReturnValidXMLFromArray()
    {
        $this->markTestIncomplete('das');
        $data = [
            //'@attributes' => array('id' => 12, 'name' => 'testarray'),
            //'@anot' => 'long',
            //'foo' => 'bar',
            //'fails' => '& foo = bar ><%',
            //'baz' => ['1', '2'],
            'number' => [1, 2, 5],
            //'floats' => [1.0, 3.5, 0.234, 5.43, 3.3, 4.4],
            //'tsst' => ['tokenize' => true, 'reflect' => false, 'bla', 'blub'],
            //'testb' => ['@type' => 'string', 'text' => 1]
        ];
        $this->processor->setPluralizer(function ($value) {
            if ($value == 'number') {
                return 'numbers';
            }
            return $value;
        });
        $this->processor->setSingularizer(function ($value) {
            if ($value == 'numbers') {
                return 'number';
            }
            return $value;
        });
        $string = $this->processor->encode($data);
        //var_dump($string);
        $xml = simplexml_load_string($string);
        $childnode = $xml->children();
        var_dump($string);
        $array = $this->processor->decode($string);
        var_dump($array);
        $string = $this->processor->encode($array['data']);
        //var_dump($array['data']);
        //var_dump($string);
        //$this->assertTrue($childnode->getName() === 'foo');
    }

    public function testFoo()
    {
        $data = [
            'data' => ['a', 'b'],
            'obj'  => new \StdClass()
            ];

        $data['obj']->name = 'foo';

        $object = new Test(array(1, 2), new \StdClass());

        $reflObj = new \ReflectionObject($object);
        $reflection = new \ReflectionClass('Stream\Tests\Serializer\Test');

        $dummy = $reflection->newInstanceWithoutConstructor();

        foreach ($reflection->getProperties() as $property) {
            $property->setAccessible(true);
            $property->setValue($dummy, $data[$property->name]);
        }
    }

    /**
     * @test
     */
    public function testConvertXmlandBack()
    {
        $file = file_get_contents(__DIR__ . '/Fixures/test.xml');
        $data = $this->processor->decode($file);
        var_dump($data); die;
        $xml =  $this->processor->encode($data['data']);
        $this->assertXmlStringEqualsXmlString($file, $xml);
    }
}
