<?php

/**
 * This File is part of the Stream\Common package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Common\Annotations;

/**
 * @Annotation
 * @Target("METHOD")
 */
class Before
{
    /**
     * expression
     *
     * @var string
     */
    public $expression;

    /**
     * __construct
     *
     * @param array $values
     */
    public function __construct(array $values)
    {
        $this->expression = isset($values['expression']) ? $values['expression'] : $values['value'];
    }
}
