<?php

/**
 * This File is part of the Stubs package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Common\Stubs;

use Stream\Common\AbstractServiceDriver;

/**
 * @class StubsServiceDriver
 */

class StubsServiceDriver extends AbstractServiceDriver
{
    /**
     * registerServices
     *
     * @access protected
     * @return array
     */
    protected function registerServices()
    {
        return [
            'foo' => $this->app->share(
                function () {
                    return 'foo';
                }
            ),

            'bar' => $this->app->share(
                function () {
                    return 'bar';
                }
            ),
            'baz' => $this->app->share(
                function () {
                    return 'baz';
                }
            )
        ];
    }
}
