<?php

/**
 * This File is part of the tests package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Common;

use Stream\Tests\Common\Stubs\StubsServiceDriver;
use org\bovigo\vfs\vfsStream;
use Mockery as m;

/**
 * @class ServiceDriverTest
 */
class ServiceDriverTest extends \PHPUnit_Framework_TestCase
{

    /**
     * createService
     *
     * @param mixed $app
     * @access protected
     * @return mixed
     */
    protected function createService($app)
    {
        return new StubsServiceDriver($app);
    }

    protected function tearDown()
    {
        m::close();
    }

    /**
     * createApp
     *
     * @access protected
     * @return mixed
     */
    protected function createApp(\Closure $constructor = null)
    {
        $app = m::mock('Stream\IoC\InterfaceContainer');

        if (!is_null($constructor)) {
            $constructor($app);
        }

        return $app;
    }

    /**
     * @test
     */
    public function testCreateService()
    {
        $service = $this->createService($this->createApp());
        $this->assertInstanceOf('Stream\Common\AbstractServiceDriver', $service);
    }

    /**
     * @test
     */
    public function testGetProvisions()
    {
        $service = $this->createService($this->createApp());
        $this->assertTrue(is_null($service->provides()));
    }

    /**
     * @test
     */
    public function testRegisterService()
    {
        $app = $this->createApp(
            function ($app) {
                $app->shouldReceive('share')->andReturn(function () {});
            }
        );

        $app->shouldReceive('offsetExists')->andReturn(false);
        $app->shouldReceive('offsetSet');

        $service = $this->createService($app);
        $service->register();

        $this->assertTrue(in_array('foo', $service->provides()));
    }


    /**
     * @test
     * @expectedException Stream\Common\Exception\ServiceRegistrationConflict
     */
    public function testRegisterServiceRaiseConflict()
    {
        $app = $this->createApp(function ($app) {
            $app->shouldReceive('share')->andReturn(function () {});
            $app->shouldReceive('offsetExists')->times(1)->andReturn(true);
        });

        $service = $this->createService($app);
        $service->register();
    }

    /**
     * @test
     */
    public function testRegisterPackage()
    {
        $lang = m::mock('lang');
        $lang->shouldReceive('registerTranslation');

        $conf = m::mock('config');
        $conf->shouldReceive('registerNamespace');

        $app = $this->createApp(function ($app) use ($lang, $conf) {
            $app->shouldReceive('offsetGet')->with('l10n')->andReturn($lang);
            $app->shouldReceive('offsetGet')->with('config')->andReturn($conf);
        });

        $service = $this->createVirtualPackageDriver($app);
        $this->assertTrue($service->package('mypackage'));
    }

    /**
     * createVirtualPackageDriver
     *
     * @param mixed $app
     * @access protected
     * @return mixed
     */
    protected function createVirtualPackageDriver($app)
    {
        $fsys = vfsStream::setup('MyPackage', 0775);
        $classContent = file_get_contents(dirname(__FILE__) . '/Stubs/ServiceDriverStub.template');

        mkdir($fsys->url() . '/config');
        mkdir($fsys->url() . '/l10n');
        mkdir($fsys->url() . '/i18n');

        file_put_contents($fsys->url() . '/MyPackageServiceDriver.php', $classContent);

        include $fsys->url() . '/MyPackageServiceDriver.php';
        return new \MyPackage\MyPackageServiceDriver($app);
    }
}
