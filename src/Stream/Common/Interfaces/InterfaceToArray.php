<?php

/**
 * This File is part of the Stream\Common package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Common\Interfaces;

/**
 * Interface ToArray
 *
 * @author Thomas Appel <mail@thomas-appel.com>
 * @version 10.0
 * @package Stream\Common\Interfaces
 * @license MIT
 */
interface InterfaceToArray
{
    public function toArray();
}
