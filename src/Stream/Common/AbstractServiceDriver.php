<?php

/**
 * This File is part of the Stream\Common package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Common;

use Stream\IoC\InterfaceContainer;
use Stream\Common\Exception\ServiceRegistrationConflict;
use Stream\Common\Interfaces\InterfaceServiceDriver;

/**
 * Class: AbstractServiceDriver
 *
 * @abstract
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
abstract class AbstractServiceDriver implements InterfaceServiceDriver
{
    const L_DEFERRED = -1;
    /**
     * app
     *
     * @var Application
     */
    protected $app;

    /**
     * provisions
     *
     * @var array
     */
    protected $provisions;

    /**
     * __construct
     *
     * @param mixed $app
     * @access public
     */
    public function __construct(InterfaceContainer $app)
    {
        $this->app = $app;
    }

    /**
     * priority
     *
     * @access public
     * @return integer
     */
    public function priority()
    {
        return 0;
    }

    /**
     * deferred
     *
     * @access public
     * @return boolean
     */
    final public function deferred()
    {
        return same(static::L_DEFERRED, $this->priority());
    }

    /**
     * register services
     *
     * Calls $this->registerServices() that must return an array
     *  containing serivce keys and serive implementations
     *
     * @access public
     *
     * @throws Stream\Common\Exception\ServiceRegistrationConflict
     * @return void
     */
    final public function register()
    {
        foreach ($services = $this->registerServices() as $service => $implementation) {

            if (isset($this->app[$service])) {
                return $this->handleRegistrationConflict($service);
            }

            $this->app[$service] = $implementation;
        }

        $this->provisions = array_keys($services);
    }

    /**
     * @{inheritDoc}
     */
    final public function package($name, $namespace = null, $path = null)
    {
        if ($path = !is_null($path) ? realpath($path) : $this->resolveServiceDriverPath()) {

            if (is_dir($config = $path . '/config')) {
                $this->app['config']->registerNamespace($name, $namespace, $path);
            }

            if (is_dir($lang = $path . '/l10n')) {
                $this->app['lang']->registerTranslation($namespace, $lang);
            }

            return true;
        }
        return false;
    }

    /**
     * registerServices
     *
     * @param array $services
     * @access protected
     * @abstract
     * @return array
     */
    abstract protected function registerServices();

    /**
     * provides
     *
     * @access public
     * @final
     * @return array
     */
    final public function provides()
    {
        return $this->provisions;
    }

    /**
     * boot a package
     *
     * @access public
     * @return mixed
     */
    public function boot()
    {
    }

    /**
     * collectServiceDrivers
     *
     * @access protected
     * @return array
     */
    protected function collectServiceDrivers()
    {

    }

    /**
     * resolveServiceDriverPath
     *
     * @access protected
     * @return mixed
     */
    protected function resolveServiceDriverPath()
    {
        $reflection = new \ReflectionObject($this);

        while (false !== $reflection) {
            $classes[]  = $reflection;
            $reflection = $reflection->getParentClass();
        }
        return dirname(array_pop($classes)->getFilename());
    }

    /**
     * handleRegistrationConflict
     *
     * @param string $service
     *
     * @access private
     * @return mixed
     */
    private function handleRegistrationConflict($service)
    {
        throw new ServiceRegistrationConflict(
            sprintf('Cannot register %s as service, %s is already registed', $service, $service)
        );
    }
}
