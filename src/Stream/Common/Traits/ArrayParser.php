<?php

/**
 * This File is part of the Stream\Common\Traits package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Common\Traits;

/**
 * @trait ArrayParser
 */
trait ArrayParser
{
    /**
     * getByKey
     *
     * @param mixed $namespace
     * @param array $array
     * @param string $separator
     * @access public
     * @return mixed
     */
    public function getByKey($namespace, array $array, $separator = '.')
    {
        $keys = explode($separator, $namespace);

        if (!isset($array[current($keys)])) {
            return;
        }

        while (count($keys) > 0) {
            $array = $array[array_shift($keys)];
        }
        return $array;
    }

    /**
     * setByKey
     *
     * @param mixed $namespace
     * @param mixed $value
     * @param array $array
     * @param string $separator
     * @access
     * @return mixed
     */
    function setByKey($namespace, $value, array &$array = [], $separator = '.')
    {
        $keys = explode($separator, $namespace);
        $key = array_shift($keys);

        if (!count($keys) && $array[$key] = $value) {
            return $array;
        }

        if (!isset($array[$key])) {
            $array[$key] = [];
        }

        return $this->setByKey(implode($separator, $keys), $value, $array[$key], $separator);
    }
}
