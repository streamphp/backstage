<?php

/**
 * This File is part of the Controller package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Routing\Controller;

use Stream\IoC\InterfaceContainer;
use Stream\Routing\Router;

/**
 * Interface: Controller
 *
 *
 * @package Stream\Routing
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
interface InterfaceController
{
    public function callAction(InterfaceContainer $container, $method, $arguments);
}
