<?php

/**
 * This File is part of the Stream\Routing package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */
namespace Stream\Routing;

use \Closure;
use Stream\IoC\InterfaceContainer;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\ExceptionInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

/**
 * @class Router
 */
class Router
{
    /**
     * routes
     *
     * @var RoutCollection
     */
    protected $routes;

    /**
     * container
     *
     * @var InterfaceContainer
     */
    protected $container;

    /**
     * __construct
     *
     * @param InterfaceContainer $container
     * @param RouteCollection $collection
     * @access public
     * @return void
     */
    public function __construct(InterfaceContainer $container, RouteCollection $collection)
    {
        $this->routes    = $collection;
        $this->container = $container;
    }

    /**
     * start
     *
     * @param Request $request
     * @access public
     * @return void
     */
    public function start(Request $request = null)
    {
        if (!is_null($request)) {
            $this->setContext($request);
            $htis->setUrlMatcher($this->urlMatcher);
        }

        return $this->resolve($this->request);
    }

    /**
     * resolve
     *
     * @param Request $request
     * @access public
     * @return mixed
     */
    protected function resolve(Request $request)
    {
        try {
            $path   = $this->fixPath($request->getPathInfo());
            $params = $this->urlMatcher->match($path);
            var_dump($params);
        } catch (ExceptionInterface $notFound) {
            $this->handleNotFoundException($notFound);
        }
        return null;
    }

    /**
     * handleNotFoundException
     *
     * @param ExceptionInterface $notFound
     * @access protected
     * @return mixed
     */
    protected function handleNotFoundException(ExceptionInterface $notFound)
    {
        var_dump($notFound);;
    }



    /**
     * getRoutes
     *
     * @access public
     * @return RouteCollection
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * getUrlMatcher
     *
     * @param UrlMatcher $matcher
     * @access public
     * @return void
     */
    public function getUrlMatcher()
    {
        return $this->urlMatcher;
    }

    /**
     * fixPath
     *
     * @param mixed $path
     * @access protected
     * @return mixed
     */
    protected function fixPath($path)
    {
        return sprintf('/%s',
            ltrim(
                (strlen($path) > 1 && str_ends_with('/', $path)) ? substr($path, 0, -1) : $path, '/'
            ));
    }


    /**
     * setRequest
     *
     * @param Request $request
     * @access public
     * @return mixed
     */
    public function setContext(RequestContext $context)
    {
        $this->context = $context;
    }

    /**
     * setRequest
     *
     * @param Request $request
     * @access public
     * @return mixed
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * setUrlMatcher
     *
     * @param UrlMatcher $matcher
     * @access public
     * @return void
     */
    public function setUrlMatcher(RouteMatcher $matcher)
    {
        $this->urlMatcher = $matcher;
        $this->urlMatcher->setRoutes($this->routes);
        $this->urlMatcher->setContext($this->context);
    }

    /**
     * getRequest
     *
     * @access public
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }



    /**
     * make
     *
     * @param string $method
     * @param string $pattern
     * @param mixed  $action
     * @access protected
     * @return void
     */
    protected function make($method, $pattern, $action)
    {
        $action = $this->parseAction($action);

        $route = new Route($pattern);
        $this->routes->add($this->getRouteName($method, $pattern), $route);
    }

    protected function getRouteName($method, $pattern)
    {
        return sprintf('%s %s', $method, $pattern);
    }

    /**
     * call
     *
     * @param mixed $route
     * @access public
     * @return mixed
     */
    public function call($route)
    {
        $resource = $this->resolve($route);
    }

    /**
     * get
     *
     * @param mixed $pattern
     * @param mixed $action
     * @access public
     * @return mixed
     */
    public function get($pattern, $action)
    {
        return $this->make(__FUNCTION__, $pattern, $action);
    }

    /**
     * post
     *
     * @param mixed $pattern
     * @param mixed $action
     * @access public
     * @return void
     */
    public function post($pattern, $action)
    {
        return $this->make(__FUNCTION__, $pattern, $action);
    }

    /**
     * put
     *
     * @param mixed $pattern
     * @param mixed $action
     * @access public
     * @return void
     */
    public function put($pattern, $action)
    {
        return $this->make(__FUNCTION__, $pattern, $action);
    }

    /**
     * delete
     *
     * @param mixed $pattern
     * @param mixed $action
     * @access public
     * @return void
     */
    public function delete($pattern, $action)
    {
        return $this->make(__FUNCTION__, $pattern, $action);
    }

    /**
     * patch
     *
     * @param mixed $pattern
     * @param mixed $action
     * @access public
     * @return void
     */
    public function patch($pattern, $action)
    {
        return $this->make(__FUNCTION__, $pattern, $action);
    }

    /**
     * any
     *
     * @param mixed $param
     * @access public
     * @return void
     */
    public function any($pattern, $action)
    {
        return $this->make('*', $pattern, $action);
    }

    /**
     * parseAction
     *
     * @param mixed $action
     * @access protected
     * @return mixed
     */
    protected function parseAction($action)
    {
        if ($action instanceof Closure) {
            return [$action];
        }

        if (is_string($action)) {
            return [$this->parseControllerCallback($action)];
        }
    }

    /**
     * parseControllerCallback
     *
     * @param mixed $controllerAction
     * @param mixed $fallbackmethod
     * @access protected
     * @return Closure
     */
    protected function parseControllerCallback($controllerAction, $fallbackmethod = null)
    {
        return $this->container->share(function () use ($controllerAction)
        {
            list($controller, $method) = explode('@', $controllerAction);

            $route = $this->getCurrentRoute();
            $args  = $route->getArguments();

            return call_user_func_array([
                $this->container->resolve($controller),
                is_null($method) ? $fallbackmethod : $method
                ], [$this->container, $this, $arg]
            );
        });
    }
}
