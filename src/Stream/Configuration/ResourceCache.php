<?php

/**
 * This File is part of the Stream\Configuration package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Configuration;

/**
 * Class: ResourceCache
 *
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class ResourceCache
{
    /**
     * filecache
     *
     * @var mixed
     */
    protected $filecache;

    /**
     * name
     *
     * @var mixed
     */
    protected $name;

    /**
     * __construct
     *
     * @param FileStorage $storage
     * @access public
     * @return mixed
     */
    public function __construct(FileStorage $storage, $resourceName)
    {
        $this->filecache = $storage;
        $this->name      = $resourceName;
        $this->setCreationDate();
    }

    /**
     * setCreationDate
     *
     * @param mixed $param
     * @access protected
     * @return void
     */
    protected function setCreationDate()
    {
        if ($crdate = $this->filecache->getCreationDate($this)) {
            $this->created = $crdate;
        } else {
            $this->created = 0;
        }
    }

    /**
     * fileChanged
     *
     * @param array $files
     * @access public
     * @return boolean
     */
    public function fileChanged(array $files)
    {
        foreach ($files as $file) {
            if ($this->created < $file->lastModified()) {
                return true;
            }
        }
        return false;
    }

    /**
     * __call
     *
     * @param mixed $method
     * @param mixed $arguments
     * @access public
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        return call_user_func_array([$this->filecache, $method], $arguments);
    }

    /**
     * writeCacheFile
     *
     * @param mixed $data
     * @access protected
     * @return void
     */
    protected function writeCacheFile(array $data)
    {
        $this->filecache->write($this->name);
        $this->setCreationDate();
    }

    /**
     * readCacheFile
     *
     * @param mixed $data
     * @access protected
     * @return mixed
     */
    protected function readCacheFile()
    {
        return $this->filecache->read($this->name);
    }
}
