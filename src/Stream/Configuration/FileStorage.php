<?php

/**
 * This File is part of the Stream\Configuration package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Configuration;

use Stream\Cache\Storage;

/**
 * Class: FileStorage
 *
 * @implements InterfaceFileStorage
 * @implements InterfaceFileStorageDriver
 * @uses Storage
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class FileStorage extends Storage implements InterfaceFileStorage
{
    /**
     * __construct
     *
     * @param DriverFileStorage $cachDriver
     * @param string $prefix
     * @access public
     * @return mixed
     */
    public function __construct(FileStorageDriver $cachDriver, $prefix = 'file_cache')
    {
        parent::__construct($cachDriver, $prefix);
    }

    /**
     * getCachedir
     *
     * @access public
     * @return Stream\Filesystem\Filesystem
     */
    public function getCachedir()
    {
        return $this->cacheDriver->getCachedir();
    }

    /**
     * getCreationDate
     *
     * @param string $cachid
     * @access public
     * @return null|int
     */
    public function getCreationDate($cachid)
    {
        if ($this->has($cachid)) {
            $cachid = $this->getCacheID($cachid);
            return $this->getCachedir()->get($cachid)->lastModified();
        }
    }

}
