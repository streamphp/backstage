<?php

/**
 * This File is part of the Stream\Configuration package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Configuration;

/**
 * Interface FileStorage
 *
 * @uses IntefaceStorage
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
interface InterfaceFileStorage extends InterfaceFileStorageDriver
{
    public function getCreationDate($cachid);
}
