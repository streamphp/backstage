<?php

/**
 * This File is part of the Stream\Configuration package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Configuration;

use Stream\Cache\Driver\DriverFilesystem;

/**
 * @class FileStorageDriver
 */

class FileStorageDriver extends DriverFilesystem implements InterfaceFileStorageDriver
{
    /**
     * getCacheDir
     *
     * @access public
     * @return Stream\Filesystem\Filesystem
     */
    public function getCacheDir()
    {
        return $this->cachedir;
    }
}
