<?php

/**
 * This File is part of the Stream\Configuration package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Configuration;

use Stream\Filesystem\FSFile;
/**
 * @class Configuration
 */
class Configuration extends AbstractSegmentParser implements \ArrayAccess
{
    /**
     * locator
     *
     * @var FileLocator
     */
    protected $locator;

    /**
     * cache
     *
     * @var ResourceCache
     */
    protected $cache;

    /**
     * items
     *
     * @var array
     */
    protected $items = [];

    /**
     * pagckages
     *
     * @var array
     */
    protected $packages = [];

    /**
     * __construct
     *
     * @param FileLocator $locator
     * @param ResourceCache $cache
     * @param XMLProcessor $parser
     * @access public
     * @return mixed
     */
    public function __construct(FileLocator $locator,  ResourceCache $cache)
    {
        $this->cache     = $cache;
        $this->locator   = $locator;
    }


    public function offsetExists($param)
    {
        return null;
    }

    public function offsetGet()
    {
        // body
    }

    public function offsetSet()
    {
        // body
    }

    public function offsetUnset($param)
    {
        return null;
    }

}
