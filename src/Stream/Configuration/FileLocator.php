<?php

/**
 * This File is part of the Stream\Configuration package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Configuration;

use Stream\Filesystem\FSDirectory;
use Stream\Serializer\Processor\XMLProcessor;

/**
 * @class FileLocator
 */

class FileLocator
{
    /**
     * allowedTypes
     *
     * @var array
     */
    protected static $allowedTypes = ['xml', 'php'];

    /**
     * root
     *
     * @var string
     */
    protected $root;

    /**
     * processor
     *
     * @var XMLProcessor
     */
    protected $processor;

    /**
     * locations
     *
     * @var array
     */
    protected $locations = [];

    /**
     * __construct
     *
     * @param FSDirectory $location
     * @access public
     * @return mixed
     */
    public function __construct(Filesystem $files, XMLProcessor $processor, $root)
    {
        $this->filesystem = $files;
        $this->processor  = $processor;
        $this->root       = $this->filesystem->get($root);
    }

    /**
     * isAllowedFile
     *
     * @param mixed $file
     * @access public
     * @return mixed
     */
    public function isAllowedFile($file)
    {
        if ($file = $this->location->get($file)) {
            return in_array($file->extenison, static::$allowedTyes);
        }
        return false;
    }

    /**
     * includeFile
     *
     * @param FSFile $file
     * @access protected
     * @return mixed
     */
    protected function includeFile(FSFile $file)
    {
        return include $file->path;
    }

    /**
     * getContents
     *
     * @access public
     * @return mixed
     */
    public function getContents()
    {
        $contents = [];

        foreach ($this->files as $file) {
            $contents[] = $this->getFileContents($file);
        }
    }

    /**
     * load
     *
     * @param mixed $env
     * @param mixed $group
     * @param mixed $namespace
     * @access public
     * @return mixed
     */
    public function load($env, $group, $namespace = null)
    {
        $lookups = $this->getPaths($namespace);

    }

    /**
     * getPaths
     *
     * @param mixed $namespace
     * @access public
     * @return mixed
     */
    public function getPaths($namespace = null)
    {
        if (!is_null($namespace)) {
            if (isset($this->namespace[$namespace])) {
                return $this->filesystem->get($this->namespace[$namespace]);
            }
        }
        return $this->root;
    }


    /**
     * addNamespace
     *
     * @param mixed $namespace
     * @param mixed $definition
     * @access public
     * @return mixed
     */
    public function addNamespace($namespace, $definition)
    {
        $this->namespace[$namespace] = $definition;
    }

    /**
     * getFileContents
     *
     * @param mixed $file
     * @access protected
     * @return array
     */
    protected function getFileContents($file)
    {
        switch ($file->extension) {
            case 'xml':
                return $this->processor->encode($file->contents());
                break;
            case 'php':
                return $this->includeFile($file);
                break;
        }
        return [];
    }
}
