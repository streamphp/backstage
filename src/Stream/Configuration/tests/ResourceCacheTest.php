<?php

/**
 * This File is part of the Stream\Configuration\tests package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Configuration;

use Mockery as m;
use org\bovigo\vfs\vfsStream;
use Stream\Configuration\ResourceCache;

/**
 * Class: ResourceCacheTest
 *
 * @uses \PHPUnit_Framework_TestCase
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class ResourceCacheTest extends \PHPUnit_Framework_TestCase
{
    protected $root;
    protected $rootPath;
    protected $appPath;

    protected function setUp()
    {
        $this->root = vfsStream::setup('root', 0777);
        $this->rootpath = vfsStream::url('root');
        mkdir($this->rootpath . '/application/config/cache', 0755, true);
        $this->appPath = $this->rootpath . '/application';
        $this->makeResourceCache();
    }

    protected function tearDown()
    {
        m::close();
    }

    public function testCacheWarmUpCache()
    {
        $this->fileCache->shouldReceive('getCreationDate')->andReturn(null);
        $resourceCache = new ResourceCache($this->fileCache, 'resource');
        $file = m::mock('Stream\Filesystem\FSDirectory');
        $file->shouldReceive('lastModified')->andReturn(time());

        $this->assertTrue($resourceCache->fileChanged([$file]));
    }

    public function testFileChangeDetect()
    {
        $crdate = time() - 10;

        $this->fileCache->shouldReceive('getCreationDate')->andReturn(time());
        $resourceCache = new ResourceCache($this->fileCache, 'resource');
        $file = m::mock('Stream\Filesystem\FSDirectory');
        $file->shouldReceive('lastModified')->andReturn($crdate);
        $file2 = clone($file);

        $this->assertFalse($resourceCache->fileChanged([$file, $file2]));
    }

    protected function makeResourceCache()
    {
        $this->fileCache = m::mock('Stream\Configuration\FileStorage');
        $this->cacheDir  = m::mock('Stream\Filesystem\FSDirectory');
        $this->cacheDir->path = $this->rootpath . '/application/config/cache';
        return null;
    }


}
