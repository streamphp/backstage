<?php

/**
 * This File is part of the Stream\Configuration package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Configuration;

use Stream\Serializer\Processor\XMLProcessor;
use Stream\Common\AbstractServicDriver as ServiceDriver;

/**
 * Class: ConfigurationServiceDriver
 *
 * @uses ServiceDriver
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class ConfigurationServiceDriver extends ServiceDriver
{
    /**
     * {@inheritDoc}
     */
    protected function registerServices()
    {
        $services = [];
        $this->registerResourceCacheService($services);
        $this->registerFileLocatorService($services);

        $service['config'] = $this->app->share(function () use (&$services)
        {
            $processor = new XMLProcessor;
            new Configuration($service['config.resources'], $service['config.cache'], $processor);
        });
        return $services;
    }

    /**
     * registerFileLocatorService
     *
     * @param array $services
     * @access protected
     * @return void
     */
    protected function registerFileLocatorService(array &$services)
    {
        $app = $this->app;
        $services['config.resources'] = $this->app->share(function () use (&$app, &$services)
        {
            return new FileLocator($app['filesystem']);
        });
    }


    /**
     * registerResourceCacheService
     *
     * @param array $services
     * @access protected
     * @return void
     */
    protected function registerResourceCacheService(array &$services)
    {
        $services['config.cache'] = $this->app->share(function () use (&$app, &$services)
        {
            $cachedir = $app['filesystem']->get('application/config/cache');
            $cache = new FileStorage(new FileStorageDriver($cachedir), 'config');

            return new ResourceCache($cache, 'resource');
        });
    }
}
