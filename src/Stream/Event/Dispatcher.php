<?php

/**
 * This File is part of the Stream\Event package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Event;

use Stream\Event\Event;
use Stream\IoC\Container;
use Stream\Common\Traits\ArrayParser;
use Stream\Common\AbstractSegmentParser as SegmentParser;
use Symfony\Component\EventDispatcher\Event as SymfonyEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class: Dispatcher
 *
 * @see EventDispatcherInterface
 * @author Thomas Appel <mail@thomas-appel.com>
 *
 * @package Stream\Event
 * @version 1.0.0
 */
class Dispatcher extends SegmentParser implements EventDispatcherInterface
{

    use ArrayParser;

    /**
     * namespaces
     *
     * @var mixed
     */
    protected $namespaces = [];

    /**
     * listeners
     *
     * @var mixed
     */
    protected $listeners  = [];

    /**
     * count
     *
     * @var mixed
     */
    protected $count = [];

    /**
     * callbacks
     *
     * @var mixed
     */
    protected $callbacks = [];

    /**
     * container
     *
     * @var mixed
     */
    protected $container;

    /**
     * namespaceSeparator
     *
     * @var string
     */
    protected static $namespaceSeparator = '@';

    /**
     * __construct
     *
     * @param Stream\IoC\Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * on
     *
     * @param string          $event
     * @param mixed|callable  $listener
     * @param int             $prioriy
     * @return void
     */
    public function on($event, $listener, $prioriy = 0)
    {
        if (is_string($listener)) {
            $listener = $this->bindClassCallback($listener);
        }

        if (!is_callable($listener)) {
            throw new \InvalidArgumentException(
                sprintf('%s expects 2nd argument to be callable, %s given.', __FUNCTION__, gettype($listener))
            );
        }

        if (!is_int($prioriy)) {
            throw new \InvalidArgumentException(
                sprintf('%s expects 3rd argument to be an integer, %s given', __FUNCTION__, gettype($prioriy))
            );
        }

        $evd = $this->getNSDetails($event);

        if (!isset($this->listeners[$evd['ns']])) {
            $this->listeners[$evd['ns']] = [];
        }

        if (!isset($this->count[$event])) {
            $this->count[$event] = 0;
        }
        $this->count[$event]++;

        $hash = $this->getItemID($listener);

        $this->listeners[$evd['ns']][$prioriy][] = [
            'originalEvent' => $event,
            'name'          => $evd['name'],
            'namespace'     => $evd['ns'],
            'callback'      => &$listener,
            'handlerid'     => $hash
        ];
    }

    /**
     * once
     *
     * @param mixed $event
     * @param mixed $listener
     * @param int $prioriy
     * @return void
     */
    public function once($event, $listener, $prioriy = 0)
    {
        $callback = null;

        if (is_string($listener)) {
            $listener = $this->bindClassCallback($listener);
        }

        $callback = function () use (&$listener, &$callback, $event) {
            $this->off($event, $callback);
            $arguments = func_get_args();
            return call_user_func_array($listener, $arguments);
        };

        return $this->on($event, $callback, $prioriy);
    }

    /**
     * off
     *
     * @param mixed $event
     * @param mixed $listener
     * @return void
     */
    public function off($event, $listener = null)
    {
        $evd = $this->getNSDetails($event);


        if (is_null($listener) && $evd['ns'] === $evd['name']) {
            unset($this->listeners[$evd['ns']]);
            unset($this->count[$event]);
            return;
        }

        foreach ($this->listeners[$evd['ns']] as $i => &$l) {
            foreach ($l as $j => &$listnr) {

                if (
                    (!isset($evd['name']) || ($evd['name'] === $listnr['name'])) &&
                    (is_null($listener) || ($listnr['callback'] === $listener))
                ) {
                    unset($l[$j]);
                }
            }

            if (empty($l)) {
                $l = null;
                unset($this->listeners[$evd['ns']][$i]);
            }
        }
        if (empty($this->listeners[$evd['ns']])) {
            unset($this->listeners[$evd['ns']]);
        }

        $this->count[$event]--;

        if ($this->count[$event] < 1) {
            unset($this->count[$event]);
        }
    }

    /**
     * trigger
     *
     * @param mixed $event
     * @param mixed $context
     * @param mixed $stop
     * @access public
     * @return mixed
     */
    public function trigger($event, $context = null, $stop = false)
    {
        $evd = $this->getNSDetails($event);

        if (!isset($this->listeners[$evd['ns']])) {
            return;
        }

        $responses = [];


        ksort($this->listeners[$evd['ns']]);

        foreach ($this->getListeners($event) as $listener) {

            if (isset($evd['name']) && ($listener['name'] !== $evd['name'])) {
                continue;
            }

            $cb = &$listener['callback'];
            $cbContext = is_array($context) ? $context : [$context];

            if ($response = call_user_func_array($cb, $cbContext)) {
                $responses[] = $response;
            }

            if ($stop && count($responses)) {
                break;
            }

            $crContext = current($cbContext);

            if ($crContext instanceof Event && $crContext->isPropagationStopped()) {
                break;
            }
        }

        return empty($responses) ? null : $responses;
    }

    /**
     * until
     *
     * @param mixed $event
     * @param mixed $context
     */
    public function until($event, $context = null)
    {
        return $this->trigger($event, $context, true);
    }


    /**
     * dispatch
     *
     * @param mixed $eventName
     * @param SymfonyEvent $event
     */
    public function dispatch($eventName, SymfonyEvent $event = null)
    {
        return $this->trigger($eventName, $event);
    }

    /**
     * addListener
     *
     * @param mixed $eventName
     * @param mixed $listener
     * @param int $priority
     */
    public function addListener($eventName, $listener, $priority = 0)
    {
        return $this->on($eventName, $listener, $priority);
    }

    /**
     * addSubscriber
     *
     * @param EventSubscriberInterface $subscriber
     */
    public function addSubscriber(EventSubscriberInterface $subscriber)
    {
        $id = $this->getItemID($subscriber);
        $this->subscribers[$id] = $subscriber;
        $this->handleSubscriber($subscriber, 'on');
    }

    /**
     * removeSubscriber
     *
     * @param EventSubscriberInterface $subscriber
     */
    public function removeSubscriber(EventSubscriberInterface $subscriber)
    {
        $id = $this->getItemID($subscriber);

        if (isset($this->subscribers[$id])) {
            unset($this->subscribers[$id]);
            $this->handleSubscriber($subscriber, 'off');
        }
    }

    /**
     * handleSubscriber
     *
     * @param mixed $subscriber
     * @param string $type
     */
    protected function handleSubscriber($subscriber, $type = 'on')
    {
        foreach ($subscriber::getSubscribedEvents() as $eventName => $events) {

            foreach ($events as $i => $evt) {

                if (!is_array($evt)) {
                    $prio = isset($events[1]) ? $events[1] : $i;
                    $this->{$type}($eventName, [$subscriber, $events[0]], $prio);
                    break;
                }

                $prio = isset($evt[1]) ? $evt[1] : $i;
                $this->{$type}($eventName, [$subscriber, $evt[0]], $prio);
            }
        }
    }

    /**
     * removeListener
     *
     * @param mixed $eventName
     * @param mixed $listener
     */
    public function removeListener($eventName, $listener)
    {
        return $this->off($eventName, $listener);
    }

    /**
     * hasListeners
     *
     * @param mixed $eventName
     */
    public function hasListeners($eventName = null)
    {
        if (is_null($eventName)) {
            return count($this->listeners) > 0;
        }
        return isset($this->count[$eventName]);
    }

    /**
     * getListeners
     *
     * @param mixed $eventName
     */
    public function getListeners($eventName = null)
    {
        $out = [];
        $evd = $this->getNSDetails($eventName);



        if (is_null($eventName)) {
            foreach ($this->listeners as &$evts) {
                foreach ($evts as &$prio) {
                    foreach ($prio as &$listener) {
                        $out[] =& $listener;
                    }
                }
            }
            return $out;
        }

        foreach ($this->listeners[$evd['ns']] as &$prio) {
            foreach ($prio as &$listener) {
                $out[] =& $listener;
            }
        }

        return $out;
    }

    /**
     * getItemID
     *
     * @param Object $observer
     * @return mixed|null|string
     */
    protected function getItemID($object)
    {
        return is_object($object) ? spl_object_hash($object) : (is_array($object) ? hash('md5', serialize($object)) : null);
    }

    /**
     * getNSDetails
     *
     * @param mixed $evt
     * @return array
     */
    protected function getNSDetails($evt)
    {
        if (!isset($this->namespaces[$evt])) {
            $keys = $this->parse($evt);
            $this->namespaces[$evt] = ['ns' => $keys[1], 'name' => $keys[2]];
        }
        return $this->namespaces[$evt];
    }

    /**
     * bindClassCallback
     *
     * @param mixed $classDescriptor
     * @return Closure
     */
    protected function bindClassCallback($classDescriptor)
    {
        $container = $this->container;
        return function () use ($container, $classDescriptor) {
            list($class, $method) = $this->parse($classDescriptor);

            $method = is_null($method) ? 'handleEvent' : $method;
            $instance = $container->resolve($class);
            $arguments = func_get_args();
            return call_user_func_array([$instance, $method], $arguments);
        };
    }
}
