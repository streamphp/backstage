<?php

/**
 * This File is part of the Stream\Foundation\tests package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Foundation;

use Stream\Foundation\AbstractWrapper;
use Stream\Foundation\Application;
use Mockery as m;
use \Closure;

/**
 * Class: WrapperTest
 *
 * @uses PHPUnit_Framework_TestCase
 */
class WrapperTest extends \PHPUnit_Framework_TestCase
{

    protected function tearDown()
    {
        m::close();
    }

    /**
     * @test
     */
    public function testResolveObject()
    {
        $object = $this->getWrappedObjectMock('foo');
        $app = $this->getMockedApplication($object);
        FooWrapper::setApplication($app);
        $this->assertSame($object, FooWrapper::resolveWrappedObjectInstance('foo'));
    }

    /**
     * @test
     */
    public function testCallMethod()
    {
        $object = $this->getWrappedObjectMock('foo');
        $object->shouldReceive('doSomething')->andReturn('bar');

        $app = $this->getMockedApplication($object);

        FooWrapper::setApplication($app);
        $this->assertEquals('bar', FooWrapper::doSomething());
    }

    /**
     * @test
     */
    public function testCallMethodWithRefence()
    {
        $object = new FooWrapped();
        $app = $this->getMockedApplication($object);

        $reference = new \StdClass();
        FooWrapper::setApplication($app);
        $this->assertSame($reference, FooWrapper::doSomething($reference));
        $this->assertFalse($reference === FooWrapper::doSomething(clone($reference)));
    }

    /**
     * @test
     * @expectedException \BadMethodCallException
     */
    public function testHiddenMEthodCallThrowsEception()
    {
        $object = $this->getWrappedObjectMock('foo');
        $object->shouldReceive('doSomething')->andReturn('bar');

        $app = $this->getMockedApplication($object);

        FooWrapper::setApplication($app);
        FooWrapper::forbiddenMethod();
    }

    /**
     * getMockedApplication
     *
     * @param mixed $object
     * @param mixed $fail
     * @access protected
     * @return mixed
     */
    protected function getMockedApplication($object, $fail = false)
    {
        $app = m::mock('Stream\Foundation\Application');
        $app->shouldReceive('offsetExists')->andReturn(!$fail);
        $app->shouldReceive('offsetGet')->andReturn($object);
        return $app;
    }

    /**
     * getWrappedObjectMock
     *
     * @param string $name
     * @param Closure $setup
     * @access protected
     * @return mixed
     */
    protected function getWrappedObjectMock($name = 'foo', Closure $setup = null)
    {
        $mock = m::mock(ucfirst($name));

        if (!is_null($setup)) {
            $setup($mock);
        }
        return $mock;
    }
}

class FooWrapper extends AbstractWrapper
{
    protected static $invisible = ['forbiddenMethod'];

    protected static function getWrappedObjectDescriptor()
    {
        return 'foo';
    }
}

class FooWrapped
{
    public function doSomething(\StdClass &$foo)
    {
        return $foo;
    }
}
