<?php

/**
 * This File is part of the Stream\Foundation package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Foundation;

use Stream\IoC\Container as IoCContainer;
use Stream\Common\AbstractServiceDriver as ServiceDriver;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * @class Application
 */
class Application extends IoCContainer implements HttpKernelInterface
{

    /**
     * booted
     *
     * @var boolean
     */
    protected $booted;

    /**
     * __construct
     *
     * @access public
     * @return mixed
     */
    public function __construct()
    {
        $this->booted = false;

        $this->setRequest();
        $this->setEnv();
    }

    /**
     * register
     *
     * @param ServiceDriver $dirver
     * @param array $options
     * @access public
     * @return mixed
     */
    public function register(ServiceDriver $dirver, array $options = [])
    {
        return $driver->register();
    }

    /**
     * setLocale
     *
     * @access protected
     * @return mixed
     */
    public function setLocale()
    {
    }

    public function loadServices()
    {
        foreach ($this->services as $service) {
            if ($service->deferred()) {
                $this->deferredServices[] = $service;
                continue;
            }
        }
    }

    /**
     * boot
     *
     * @access public
     * @return mixed
     */
    public function boot()
    {
    }

    public function isCommandLine()
    {
        return equals('cli', php_sapi_name());
    }

    /**
     * setRequest
     *
     * @access protected
     * @return mixed
     */
    protected function setRequest()
    {
        $this['request'] = SymfonyRequest::createFromGlobals();
    }

    /**
     * setEnv
     *
     * @access protected
     * @return mixed
     */
    protected function setEnv()
    {
        $this['env'] = 'foo';
    }

    /**
     * handle
     *
     * @param Request $request
     * @param mixed $type
     * @param mixed $catch
     * @access public
     * @return mixed
     */
    public function handle(SymfonyRequest $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true)
    {
    }

}
