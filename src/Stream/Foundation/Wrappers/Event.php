<?php

/**
 * This File is part of the Stream\Foundation\Wrappers package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Foundation\Wrappers;

use Stream\Foundation\AbstractWrapper as Wrapper;

/**
 * @class events
 */
class Events extends Wrapper
{
    /**
     * {@inheritDoc}
     */
    protected static function getWrappedObjectDescriptor()
    {
        return 'events';
    }
}
