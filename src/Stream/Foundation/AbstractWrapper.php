<?php

/**
 * This File is part of the Stream\Foundation package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Foundation;

/**
 * @class Shim
 */

abstract class AbstractWrapper
{

    /**
     * app
     *
     * @var Application
     */
    protected static $app;

    /**
     * invisible
     *
     * @var mixed
     */
    protected static $invisible = [];

    /**
     * instances
     *
     * @var mixed
     */
    protected static $instances = [];

    /**
     * getWrappedObject
     *
     * @access protected
     * @return mixed
     */
    protected static function getWrappedObjectDescriptor()
    {
        throw new \Exception(sprintf('cannot directly call AbstractWrapper::%s()', __FUNCTION__));
    }

    final private function __construct()
    {}

    /**
     * resolveWrappedObject
     *
     * @param mixed $wrapped
     * @access public
     * @return mixed
     */
    public static function resolveWrappedObject($wrapped)
    {
        if (is_object($wrapped)) {
            return $wrapped;
        }
        if (!isset(static::$instances[$wrapped])) {
            if (isset(static::$app[$wrapped])) {
                static::$instances[$wrapped] = static::$app[$wrapped];
            } else {
                throw new \InvalidArgumentException(sprintf('instance of %s cannot be resolved', $wrapped));
            }
        }
        return static::$instances[$wrapped];
    }

    /**
     * reslovel
     *
     * @param mixed $name
     * @access public
     * @return mixed
     */
    public static function resolveWrappedObjectInstance($name)
    {
        return static::resolveWrappedObject(static::getWrappedObjectDescriptor($name));
    }

    /**
     * setApplication
     *
     * @param Application $app
     * @access public
     * @return mixed
     */
    public static function setApplication(Application $app)
    {
        static::$app = $app;
    }

    /**
     * isIvisibleMethod
     *
     * @param mixed $method
     * @access protected
     * @final
     * @return mixed
     */
    final protected static function isInvisibleMethod($method)
    {
        return in_array($method, static::$invisible);
    }

    /**
     * __callStatic
     *
     * @param mixed $method
     * @param mixed $arguments
     * @access public
     * @return mixed
     */
    final public static function __callStatic($method, $arguments)
    {
        if (static::isInvisibleMethod($method)) {
            throw new \BadMethodCallException(sprintf('Call on a undefined method %s', $method));
        }
        $object = static::resolveWrappedObject(static::getWrappedObjectDescriptor());

        // sometimes we need to call a method that expects an argument passed
        // by reference. To avoid overhead, we simple apply a switchstatement
        // instead of sniffing for referenced arguments;
        // Eight possible arguments should be enough.
        switch (count($arguments)) {
            case 0:
                return $object->{$method}();
            break;
            case 1:
                return $object->{$method}($arguments[0]);
            break;
            case 2:
                return $object->{$method}($arguments[0], $arguments[1], $arguments[2]);
            break;
            case 3:
                return $object->{$method}($arguments[0], $arguments[1], $arguments[2]);
            break;
            case 4:
                return $object->{$method}($arguments[0], $arguments[1], $arguments[2],
                    $arguments[3]
                );
            break;
            case 5:
                return $object->{$method}($arguments[0], $arguments[1], $arguments[2],
                    $arguments[3], $arguments[4]
                );
            break;
            case 5:
                return $object->{$method}($arguments[0], $arguments[1], $arguments[2],
                    $arguments[3], $arguments[4], $arguments[5]
                );
            break;
            case 6:
                return $object->{$method}($arguments[0], $arguments[1], $arguments[2],
                    $arguments[3], $arguments[4], $arguments[5], $arguments[6]
                );
            break;
            case 8:
                return $object->{$method}($arguments[0], $arguments[1], $arguments[2],
                    $arguments[3], $arguments[4], $arguments[5], $arguments[6], $arguments[7]
                );
                break;
            default:
                return call_user_func_array([$object, $method], $arguments);
        }
    }
}
